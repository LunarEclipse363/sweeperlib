use super::{Error, GameState, Result};
use rand::{rngs::SmallRng, FromEntropy, Rng};
use std::fmt;
use vek::Vec2;

/// Type used for coordinates within the map
pub type Coords = Vec2<usize>;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FieldStatus {
    Unrevealed,
    MarkedGuess,
    MarkedMine,
    Revealed,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FieldKind {
    Empty,
    Mine,
    Number(u32),
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Field {
    kind: FieldKind,
    status: FieldStatus,
}

impl Field {
    pub fn marked(&self) -> bool {
        match self.status {
            FieldStatus::MarkedMine => true,
            FieldStatus::MarkedGuess => true,
            _ => false,
        }
    }

    pub fn revealed(&self) -> bool {
        match self.status {
            FieldStatus::Revealed => true,
            _ => false,
        }
    }

    pub fn is_mine(&self) -> bool {
        match self.kind {
            FieldKind::Mine => true,
            _ => false,
        }
    }

    pub fn status(&self) -> FieldStatus {
        self.status
    }

    pub fn kind(&self) -> FieldKind {
        self.kind
    }
}

/// Representation of a field for the public API
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PrintableField {
    Unrevealed,
    MarkedGuess,
    MarkedMine,
    Empty,
    Mine,
    Number(u32),
}

/// Minesweeper map
#[derive(Clone, Debug)]
pub struct Map {
    map: Vec<Field>,
    size: Coords,
    mine_count: usize,
    revealed: usize,
}

impl Map {
    /// Generates an empty map with specified size
    pub fn new(size: Coords, mine_count: usize) -> Result<Self> {
        let mut v = Vec::with_capacity(size.x * size.y);
        if mine_count >= v.capacity() {
            return Err(Error::TooManyMines);
        }
        for _ in 0..(size.x * size.y) {
            v.push(Field {
                status: FieldStatus::Unrevealed,
                kind: FieldKind::Empty,
            });
        }
        Ok(Map {
            map: v,
            size,
            mine_count,
            revealed: 0,
        })
    }

    /// Function returning size of the map
    pub fn size(&self) -> Coords {
        self.size
    }

    /// Retrieves a field from given coordinates
    ///
    /// # Errors
    /// Returns an error if coordinates are out of map bounds
    pub fn get_field(&self, coords: Coords) -> Result<Field> {
        if self.is_out_of_bounds(coords) {
            return Err(Error::FieldOutOfBounds);
        }
        match self.map.get(self.coords2index(coords)) {
            Some(x) => Ok(x.clone()),
            None => Err(Error::FieldOutOfBounds),
        }
    }

    /// Retrieves a mutable reference to field at given coordinates
    ///
    /// # Errors
    /// Returns an error if coordinates are out of map bounds
    fn get_mut_field(&mut self, coords: Coords) -> Result<&mut Field> {
        if self.is_out_of_bounds(coords) {
            return Err(Error::FieldOutOfBounds);
        }
        let index = self.coords2index(coords);
        match self.map.get_mut(index) {
            Some(x) => Ok(x),
            None => Err(Error::FieldOutOfBounds),
        }
    }

    /// Converts coordinates to the index used to store data internally in a
    /// vector
    fn coords2index(&self, coords: Coords) -> usize {
        self.size.x * coords.y + coords.x
    }

    /// Converts index used internally to coordinates
    fn index2coords(&self, index: usize) -> Coords {
        Vec2::new(index % self.size.x, index / self.size.x)
    }

    /// Checks if the coordinates are out of bounds of the map
    fn is_out_of_bounds(&self, coords: Coords) -> bool {
        coords.x >= self.size.x || coords.y >= self.size.y
    }

    /// Generates the map with set number of mines
    ///
    /// Makes sure it's not an instant loss
    pub fn generate_and_start(&mut self, coords: Coords) -> GameState {
        let count = self.mine_count;
        // Generating mines
        let mut mine_count = Vec::with_capacity(count);
        let mut rng = SmallRng::from_entropy();
        let index = self.coords2index(coords);
        for _ in 0..count {
            loop {
                let x = rng.gen_range(0, self.map.len());
                if x != index && !mine_count.contains(&x) {
                    mine_count.push(x);
                    break;
                }
            }
        }
        for x in mine_count.iter() {
            self.map.get_mut(*x).unwrap().kind = FieldKind::Mine;
            self.mine_count += 1;
        }
        // Generating numbers
        for b in mine_count.iter() {
            for c in self.get_neighbours(self.index2coords(*b)) {
                let i = self.coords2index(c);
                match self.map.get(i).unwrap().kind {
                    FieldKind::Empty => {
                        self.map.get_mut(i).unwrap().kind = FieldKind::Number(1);
                    },
                    FieldKind::Number(x) => {
                        self.map.get_mut(i).unwrap().kind = FieldKind::Number(x + 1);
                    },
                    FieldKind::Mine => {},
                }
            }
        }

        self.reveal(coords)
            .expect("Internal error of map generator: reveal() method returned an error")
    }

    /// Returns rows of `PrintableField`s for use by the public API
    pub fn get_printable_rows(&self) -> Vec<Vec<PrintableField>> {
        let mut rows = Vec::new();
        for y in 0..self.size.y {
            let mut buf = Vec::new();
            for x in 0..self.size.x {
                let field = self.map.get(self.coords2index([x, y].into())).unwrap();
                buf.push(PrintableField::from(*field))
            }
            rows.push(buf);
        }
        rows
    }

    /// Function that returns neighbours of field at given coords, makes sure
    /// they aren't out of bounds of the map
    fn get_neighbours(&self, coords: Coords) -> Vec<Coords> {
        fn wrapping_add_usize_to_i32(a: usize, b: i32) -> usize {
            if b < 0 {
                a.wrapping_sub(-b as usize)
            } else {
                a.wrapping_add(b as usize)
            }
        }
        let mut coord_vec = Vec::new();
        for x in &[-1i32, 0, 1] {
            for y in &[-1i32, 0, 1] {
                let possible_neighbour = Coords::from([
                    // Using wrapping_add here makes it unnecessary to check for underflows,
                    // instead letting the value wrap and catching that with the boundary check
                    wrapping_add_usize_to_i32(coords.x, *x),
                    wrapping_add_usize_to_i32(coords.y, *y),
                ]);
                if !(x == &0 && y == &0) && !self.is_out_of_bounds(possible_neighbour) {
                    coord_vec.push(possible_neighbour)
                }
            }
        }
        coord_vec
    }

    /// Reveals the field and applies effects of that action on the game,
    /// this includes revealing empty spaces.
    /// returns resulting GameState
    pub fn reveal(&mut self, coords: Coords) -> Result<GameState> {
        match self.get_field(coords)? {
            field if field.marked() => {
                return Err(Error::CannotRevealMarked);
            },
            field if field.kind == FieldKind::Empty => {
                let mut queue = Vec::new();
                queue.push(coords);
                while let Some(queued) = queue.pop() {
                    match self.reveal_inner(queued)? {
                        GameState::Ongoing => (),
                        GameState::Ready | GameState::Failure => unreachable!(),
                        state @ GameState::Victory => return Ok(state),
                    }
                    for neighbour in self.get_neighbours(queued) {
                        match self.get_field(neighbour)?.kind {
                            FieldKind::Empty => queue.push(neighbour),
                            FieldKind::Number(_) => match self.reveal_inner(neighbour)? {
                                GameState::Ongoing => (),
                                GameState::Ready | GameState::Failure => unreachable!(),
                                state @ GameState::Victory => return Ok(state),
                            },
                            // An empty field can only have neighbours that are either empty or numbers
                            _ => unreachable!(),
                        }
                    }
                }
                Ok(GameState::Ongoing)
            },
            _ => self.reveal_inner(coords),
        }
    }

    fn reveal_inner(&mut self, coords: Coords) -> Result<GameState> {
        if self.get_field(coords)?.status != FieldStatus::Revealed {
            self.get_mut_field(coords)?.status = FieldStatus::Revealed;
            self.revealed += 1;
        }
        if self.get_field(coords)?.is_mine() {
            Ok(GameState::Failure)
        } else if self.map.len() - self.mine_count == self.revealed {
            Ok(GameState::Victory)
        } else {
            Ok(GameState::Ongoing)
        }
    }

    /// Look at documentation of `Game::quick_reveal()` for more
    /// information
    pub fn quick_reveal(&mut self, coords: Coords) -> Result<GameState> {
        if let Field {
            kind: FieldKind::Number(mine_count),
            status: FieldStatus::Revealed,
        } = self.get_field(coords)?
        {
            let mut marked_count = 0;
            let mut reveal_queue = Vec::new();
            for neighbour_coords in self.get_neighbours(coords) {
                match self.get_field(neighbour_coords)? {
                    neighbour if neighbour.status == FieldStatus::MarkedMine => {
                        marked_count += 1;
                    },
                    neighbour if neighbour.status == FieldStatus::MarkedGuess => {
                        return Err(Error::CannotQuickRevealAroundGuesses);
                    },
                    _ => {
                        reveal_queue.push(neighbour_coords);
                    },
                }
            }
            if marked_count == mine_count {
                for field in reveal_queue {
                    match self.reveal(field)? {
                        state @ GameState::Victory | state @ GameState::Failure => {
                            return Ok(state);
                        },
                        GameState::Ongoing => {},
                        GameState::Ready => unreachable!(),
                    }
                }
                Ok(GameState::Ongoing)
            } else {
                Err(Error::WrongNumberOfMarkedMines)
            }
        } else {
            Err(Error::CanOnlyQuickRevealRevealedNumberFields)
        }
    }

    /// Marks a field as containing a mine
    pub fn mark_mine(&mut self, coords: Coords) -> Result<()> {
        if self.get_field(coords)?.revealed() {
            return Err(Error::CannotMarkRevealed);
        }
        self.get_mut_field(coords)?.status = FieldStatus::MarkedMine;
        Ok(())
    }

    /// Marks a field as maybe containing a mine, usually represented by a
    /// question mark
    pub fn mark_guess(&mut self, coords: Coords) -> Result<()> {
        if self.get_field(coords)?.revealed() {
            return Err(Error::CannotMarkRevealed);
        }
        self.get_mut_field(coords)?.status = FieldStatus::MarkedGuess;
        Ok(())
    }

    /// Unmarks a field
    pub fn unmark_field(&mut self, coords: Coords) -> Result<()> {
        if self.get_field(coords)?.marked() {
            self.get_mut_field(coords)?.status = FieldStatus::Unrevealed;
            Ok(())
        } else {
            Err(Error::CannotUnmarkNotMarked)
        }
    }

    /// Retrieves a field from given coordinates
    ///
    /// # Errors
    /// Returns an error if coordinates are out of map bounds
    pub fn get_printable_field(&self, coords: Coords) -> Result<PrintableField> {
        if self.is_out_of_bounds(coords) {
            return Err(Error::FieldOutOfBounds);
        }
        let field = self.map.get(self.coords2index(coords)).unwrap();

        Ok(PrintableField::from(*field))
    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (n, x) in self.map.iter().enumerate() {
            if n % self.size.x == 0 && n > 0 {
                writeln!(f, "")?;
            }
            write!(f, "{} ", x)?;
        }
        Ok(())
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ", match self.status {
            FieldStatus::Unrevealed => "▓".to_owned(),
            FieldStatus::MarkedGuess => "?".to_owned(),
            FieldStatus::MarkedMine => "🏴".to_owned(),
            FieldStatus::Revealed => match self.kind {
                FieldKind::Empty => "░".to_owned(),
                FieldKind::Mine => "◉".to_owned(),
                FieldKind::Number(n) => format!("{}", n),
            },
        })?;
        Ok(())
    }
}

impl fmt::Display for PrintableField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ", match self {
            PrintableField::Unrevealed => "▓".to_owned(),
            PrintableField::MarkedGuess => "?".to_owned(),
            PrintableField::MarkedMine => "🏴".to_owned(),
            PrintableField::Empty => "░".to_owned(),
            PrintableField::Mine => "◉".to_owned(),
            PrintableField::Number(n) => format!("{}", n),
        })?;
        Ok(())
    }
}

impl From<Field> for PrintableField {
    fn from(field: Field) -> Self {
        match field.status {
            FieldStatus::Unrevealed => PrintableField::Unrevealed,
            FieldStatus::MarkedGuess => PrintableField::MarkedGuess,
            FieldStatus::MarkedMine => PrintableField::MarkedMine,
            FieldStatus::Revealed => match field.kind {
                FieldKind::Empty => PrintableField::Empty,
                FieldKind::Mine => PrintableField::Mine,
                FieldKind::Number(n) => PrintableField::Number(n),
            },
        }
    }
}
