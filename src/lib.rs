//! A library containing all the game logic needed for a minesweeper game
use std::{
    fmt,
    time::{Duration, Instant},
};

mod map;
use map::Map;
pub use map::{Coords, PrintableField as Field};

/// The various error types methods can return
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Error {
    /// Requested field is outside of the map
    FieldOutOfBounds,
    /// Cannot reveal a marked field
    CannotRevealMarked,
    /// Cannot mark a field that's already revealed
    CannotMarkRevealed,
    /// Cannot unmark a field that's not marked
    CannotUnmarkNotMarked,
    /// The number of mines requested to generate was larger or equal to the
    /// amount of fields in the map
    TooManyMines,
    /// QuickReveal only works for revealed number fields
    CanOnlyQuickRevealRevealedNumberFields,
    /// Can't quick reveal if there are any guess marks around the field
    CannotQuickRevealAroundGuesses,
    /// This occurs when the number of marked mines around a field is not equal to the number shown on
    /// the field, this error applies to QuickReveal
    WrongNumberOfMarkedMines,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::FieldOutOfBounds => write!(f, "Requested field is outside of the map"),
            Error::CannotRevealMarked => write!(f, "Cannot reveal a marked field"),
            Error::CannotMarkRevealed => write!(f, "Cannot mark a revealed field"),
            Error::CannotUnmarkNotMarked => write!(f, "Cannot unmark a field that isn't marked"),
            Error::TooManyMines => write!(
                f,
                "The requested number of mines to generate was larger or equal to the amount of \
                 fields in the map"
            ),
            Error::CanOnlyQuickRevealRevealedNumberFields => {
                write!(f, "Can only quick reveal revealed number fields")
            },
            Error::CannotQuickRevealAroundGuesses => write!(
                f,
                "Cannot quick reveal when any of the surrounding fields are marked as guess"
            ),
            Error::WrongNumberOfMarkedMines => write!(
                f,
                "The number of mines the player has marked must be equal to the number on the field"
            ),
        }
    }
}

impl std::error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;

/// Representation of the current general state of the game
#[derive(Clone, Copy, Debug)]
pub enum GameState {
    /// Game was created and is ready to be started, time isn't counted yet
    Ready,
    /// Game is ongoing, the start time was saved
    Ongoing,
    /// Won the game, start time and duration was saved
    Victory,
    /// Lost the game, start time and duration was saved
    Failure,
}

/// A struct representing a single instance of a minesweeper game
#[derive(Clone, Debug)]
pub struct Game {
    map: Map,
    state: GameState,
    start_time: Option<Instant>,
    duration: Option<Duration>,
}

impl Game {
    /// Creates a new [`Game`] instance
    ///
    /// # Errors
    /// Returns an error if the number of mines is larger or equal to the field
    /// count in the map
    pub fn new(size: Coords, mines: usize) -> Result<Self> {
        Ok(Self {
            map: Map::new(size, mines)?,
            state: GameState::Ready,
            start_time: None,
            duration: None,
        })
    }

    /// Returns a [`Vec`] of rows of [`Field`]s
    pub fn get_rows(&self) -> Vec<Vec<Field>> {
        self.map.get_printable_rows()
    }

    /// Returns a single [`Field`] from the specified [`Coords`]
    /// 
    /// # Errors
    /// Returns an error if the specified coordinates are outside of the map bounds
    pub fn get_field(&self, coords: Coords) -> Result<Field> {
        self.map.get_printable_field(coords)
    }

    /// Meant for debugging, prints the map to console
    pub fn print_map(&self) {
        println!("{}", self.map);
    }

    /// Meant for debugging, returns a [`String`] containing formatted map
    pub fn map_to_string(&self) -> String {
        format!("{}", self.map)
    }

    /// Returns the current [`GameState`]
    pub fn state(&self) -> GameState {
        self.state
    }

    /// Returns size of the game map
    pub fn map_size(&self) -> Coords {
        self.map.size()
    }

    /// Marks a field as containing a mine. This prevents the field from being
    /// revealed, and counts as a marked bomb for quick reveal.
    ///
    /// # Errors
    /// Returns an error if the specified coordinates are outside of the map
    /// bounds
    pub fn mark_mine(&mut self, coords: Coords) -> Result<()> {
        self.map.mark_mine(coords)
    }

    /// Marks a field as maybe containing a mine, usually represented by a
    /// question mark symbol. This prevents the field from being revealed, but
    /// doesn't count as a marked bomb for quick reveal.
    ///
    /// # Errors
    /// Returns an error if the specified coordinates are outside of the map
    /// bounds
    pub fn mark_guess(&mut self, coords: Coords) -> Result<()> {
        self.map.mark_guess(coords)
    }

    /// Unmarks a field
    ///
    /// # Errors
    /// Returns an error if the specified coordinates are outside of the map
    /// bounds
    pub fn unmark_field(&mut self, coords: Coords) -> Result<()> {
        self.map.unmark_field(coords)
    }

    /// Reveals a field, applies all the effects it has on the game and returns
    /// the resulting GameState
    /// If the game wasn't started yet, starts it. Might be an instant victory
    /// with sufficiently small map and sufficiently little bombs. Will
    /// never be an instant loss.
    ///
    /// # Errors
    /// Returns an error if the specified coordinates are outside of the map
    /// bounds
    pub fn reveal(&mut self, coords: Coords) -> Result<GameState> {
        match self.start_time {
            None => {
                self.start_time = Some(Instant::now());
                Ok(self.map.generate_and_start(coords))
            },
            Some(_) => self.map.reveal(coords),
        }
    }

    /// If the number of bombs specified by the field is marked around it,
    /// reveal all unmarked fields around it. Will not work if at least one
    /// of the fields around it is marked as guess.
    pub fn quick_reveal(&mut self, coords: Coords) -> Result<GameState> {
        self.map.quick_reveal(coords)
    }
}
